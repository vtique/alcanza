//
//  Generated file. Do not edit.
//

// clang-format off

#import "GeneratedPluginRegistrant.h"

#if __has_include(<amplify_analytics_pinpoint/AmplifyAnalyticsPinpointPlugin.h>)
#import <amplify_analytics_pinpoint/AmplifyAnalyticsPinpointPlugin.h>
#else
@import amplify_analytics_pinpoint;
#endif

#if __has_include(<amplify_api/AmplifyApiPlugin.h>)
#import <amplify_api/AmplifyApiPlugin.h>
#else
@import amplify_api;
#endif

#if __has_include(<amplify_auth_cognito/AuthCognito.h>)
#import <amplify_auth_cognito/AuthCognito.h>
#else
@import amplify_auth_cognito;
#endif

#if __has_include(<amplify_core/AmplifyCorePlugin.h>)
#import <amplify_core/AmplifyCorePlugin.h>
#else
@import amplify_core;
#endif

#if __has_include(<amplify_flutter/Amplify.h>)
#import <amplify_flutter/Amplify.h>
#else
@import amplify_flutter;
#endif

#if __has_include(<file_picker/FilePickerPlugin.h>)
#import <file_picker/FilePickerPlugin.h>
#else
@import file_picker;
#endif

#if __has_include(<flutter_keyboard_visibility/FlutterKeyboardVisibilityPlugin.h>)
#import <flutter_keyboard_visibility/FlutterKeyboardVisibilityPlugin.h>
#else
@import flutter_keyboard_visibility;
#endif

#if __has_include(<mati_plugin_flutter/MatiPluginFlutterPlugin.h>)
#import <mati_plugin_flutter/MatiPluginFlutterPlugin.h>
#else
@import mati_plugin_flutter;
#endif

#if __has_include(<sqflite/SqflitePlugin.h>)
#import <sqflite/SqflitePlugin.h>
#else
@import sqflite;
#endif

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [AmplifyAnalyticsPinpointPlugin registerWithRegistrar:[registry registrarForPlugin:@"AmplifyAnalyticsPinpointPlugin"]];
  [AmplifyApiPlugin registerWithRegistrar:[registry registrarForPlugin:@"AmplifyApiPlugin"]];
  [AuthCognito registerWithRegistrar:[registry registrarForPlugin:@"AuthCognito"]];
  [AmplifyCorePlugin registerWithRegistrar:[registry registrarForPlugin:@"AmplifyCorePlugin"]];
  [Amplify registerWithRegistrar:[registry registrarForPlugin:@"Amplify"]];
  [FilePickerPlugin registerWithRegistrar:[registry registrarForPlugin:@"FilePickerPlugin"]];
  [FlutterKeyboardVisibilityPlugin registerWithRegistrar:[registry registrarForPlugin:@"FlutterKeyboardVisibilityPlugin"]];
  [MatiPluginFlutterPlugin registerWithRegistrar:[registry registrarForPlugin:@"MatiPluginFlutterPlugin"]];
  [SqflitePlugin registerWithRegistrar:[registry registrarForPlugin:@"SqflitePlugin"]];
}

@end
